/**
 * Post
 * @namespace instashop.posts.directives
 */
(function () {
  'use strict';

  angular
    .module('instashop.posts.directives')
    .directive('post', post);

  /**
   * @namespace Post
   */
  function post() {
    /**
     * @name directive
     * @desc The directive to be returned
     * @memberOf instashop.posts.directives.Post
     */
    var directive = {
      restrict: 'E',
      scope: {
        post: '='
      },
      templateUrl: '/templates/posts/post.html'
    };

    return directive;
  }
})();