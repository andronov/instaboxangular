/**
 * Posts
 * @namespace instashop.posts.directives
 */
(function () {
  'use strict';

  angular
    .module('instashop.posts.directives')
    .directive('posts', posts);

  /**
   * @namespace Posts
   */
  function posts() {
    /**
     * @name directive
     * @desc The directive to be returned
     * @memberOf instashop.posts.directives.Posts
     */
    var directive = {
      controller: 'PostsController',
      controllerAs: 'vm',
      restrict: 'E',
      scope: {
        posts: '='
      },
      templateUrl: '/templates/posts/posts.html'
    };

    return directive;
  }
})();