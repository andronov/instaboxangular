(function () {
  'use strict';

  angular
    .module('instashop.posts', [
      'instashop.posts.controllers',
      'instashop.posts.directives',
      'instashop.posts.services'
    ]);

  angular
    .module('instashop.posts.controllers', []);

  angular
    .module('instashop.posts.directives', ['ngDialog']);

  angular
    .module('instashop.posts.services', []);
})();