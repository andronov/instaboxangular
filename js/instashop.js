(function () {
  'use strict';

  angular
    .module('instashop', [
      'instashop.config',
      'instashop.routes',
      'instashop.layout',
      'instashop.posts',
      'instashop.utils',
      'ngCookies',
      'ngResource',
      'ngSanitize',
      'ngRoute',
    ]);

  angular
    .module('instashop.config', []);

  angular
    .module('instashop.routes', ['ngRoute']);

  angular
    .module('instashop')
    .run(run);

  run.$inject = ['$http'];

  /**
   * @name run
   * @desc Update xsrf $http headers to align with Django's defaults
   */
  function run($http) {
    $http.defaults.xsrfHeaderName = 'X-CSRFToken';
    $http.defaults.xsrfCookieName = 'csrftoken';
  }
  /*run.$inject = ['$http'];

  /**
   * @name run
   * @desc Update xsrf $http headers to align with Django's defaults
   */
  /*function run($http) {
    $http.defaults.xsrfHeaderName = 'X-CSRFToken';
    $http.defaults.xsrfCookieName = 'csrftoken';
  }*/
})();