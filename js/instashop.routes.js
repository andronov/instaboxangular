(function () {
  'use strict';

  angular
    .module('instashop.routes')
    .config(config);

  config.$inject = ['$routeProvider'];

  /**
   * @name config
   * @desc Define valid application routes
   */
  function config($routeProvider) {
    $routeProvider.when('/', {
      controller: 'IndexController', 
      controllerAs: 'vm',
      templateUrl: '/templates/layout/index.html'
    }).when('/photos/:user', {
      controller: 'PhotosController', 
      controllerAs: 'vm',
      templateUrl: '/templates/layout/photos.html'
    }).otherwise('/');
  }
})();