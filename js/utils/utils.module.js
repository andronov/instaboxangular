(function () {
  'use strict';

  angular
    .module('instashop.utils', [
      'instashop.utils.services'
    ]);

  angular
    .module('instashop.utils.services', []);
})();
