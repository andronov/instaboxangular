(function () {
  'use strict';

  angular
    .module('instashop.layout', [
      'instashop.layout.controllers'
    ]);

  angular
    .module('instashop.layout.controllers', []);
})();