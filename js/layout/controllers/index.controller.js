/**
 * IndexController
 * @namespace instashop.layout.controllers
 */
(function () {
  'use strict';

  angular
    .module('instashop.layout.controllers')
    .controller('IndexController', IndexController);

  IndexController.$inject = ['$scope','$http', 'Posts', 'Snackbar', '$cookies', '$location'];

  /**
   * @namespace IndexController
   */
  function IndexController($scope,$http, Posts, Snackbar, $cookies, $location) {
    var Results = this;
    Results.items = [];

    Results.archive = function() {
      Results.items = [];
      angular.forEach(Results, function(items) {
        Results.items.push(items);
      });
    };

    

    $scope.getResult = function(search) {
      $http({
        url: "/api/v1/test",
        method: "GET",
        params: {
          search: search
        }
      }).success(function(response) {
        console.log(response);
        Results.items = []; 
        
        for(var resp in response){
          Results.items.push(response[resp]);
        }
        //else alert("Произошла ошибка. Пожалуйста, перезагрузите страницу")
      })
    };
    Results.Enter = function(search) {
      console.log(search); 
      $scope.getResult(search)

       
      //Results.items.push({text:search});
    };

    $scope.goto = function(url) {
      $location.path(url)
    };
    
  }
})();