/**
 * NavbarController
 * @namespace instashop.layout.controllers
 */
(function () {
  'use strict';

  angular
    .module('instashop.layout.controllers')
    .controller('NavbarController', NavbarController);

  NavbarController.$inject = ['$scope'];

  /**
   * @namespace NavbarController
   */
  function NavbarController($scope) {
    /*var vm = this;

    vm.logout = logout;

    /**
     * @name logout
     * @desc Log the user out
     * @memberOf instashop.layout.controllers.NavbarController
     */
    /*function logout() {
      Authentication.logout();
    }*/
  }
})();