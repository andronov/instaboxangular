/**
 * IndexController
 * @namespace instashop.layout.controllers
 */
(function () {
  'use strict';

  angular
    .module('instashop.layout.controllers')
    .controller('PhotosController', PhotosController);

  PhotosController.$inject = ['$scope', '$routeParams', '$http', 'Posts', 'Snackbar', '$cookies', '$location', 'ngDialog'];

  /**
   * @namespace PhotosController
   */
  function PhotosController($scope, $routeParams, $http, Posts, Snackbar, $cookies, $location,ngDialog) {
    $scope.params = $routeParams, $scope.user = $scope.params.user;
    //ngDialog.open({ template: 'templateId' });

    var Results = this;
    Results.photos = [];
    Results.users = [];
    $scope.OrderList = [];

    $scope.getResultUsers = function(user) {
      $http({
        url: "/api/v1/users",
        method: "GET",
        params: {
          user: user
        }
      }).success(function(response) {
        //console.log(response);
        Results.users = []; 
        
        for(var resp in response){
          Results.users.push(response[resp]);
        }
        //else alert("Произошла ошибка. Пожалуйста, перезагрузите страницу")
      })
    };

    $scope.getResultPhotos = function(user) {
      $http({
        url: "/api/v1/users/media",
        method: "GET",
        params: {
          user: user
        }
      }).success(function(response) {
        console.log(response);
        Results.photos = []; 
        
        for(var resp in response){
          Results.photos.push(response[resp]);
        }
        //else alert("Произошла ошибка. Пожалуйста, перезагрузите страницу")
      })
    };

    $scope.FollowedForms = {
      0: 'нет подписчиков',
      one: '{} подписчик',
      two: '{} подписчика',
      three: '{} подписчика',
      four: '{} подписчика',
      other: '{} подписчиков'
    };

    $scope.FollowsForms = {
      0: 'нет подписок',
      one: '{} подписка',
      two: '{} подписки',
      three: '{} подписки',
      four: '{} подписки',
      other: '{} подписки'
    };

    
    $scope.getResultPhotos($scope.user);
    $scope.getResultUsers($scope.user);

    $scope.getCounter = function() {
      var counter = 0;
      angular.forEach($(".counter__block.active"), function(value, key){
          //console.log(parseInt($(value).find('.counter').html()));
          counter += parseInt($(value).find('.counter').html());
      });
      //console.log(counter);
      $('.counter__result').html(counter);
      $('.amount__result').html(counter*25)
    };

    $scope.RemoveOrderList  = function(id){
      if(!id){
          $scope.OrderList = [];
      };
      var active =  $('[data-is='+id+']').hasClass('active');
      angular.forEach(Results.photos, function(value, key){
          if(value.id==id){
            if($scope.OrderList.indexOf(value) == -1){
              $scope.OrderList.push(value);
            };
            if(!active){
              angular.forEach($scope.OrderList, function(value, key){
                if(value.id==id){
                  $scope.OrderList.splice(key, 1);
                };
              });
            }
          };
      });
      /*console.log($scope.OrderList[0].counter = 1);
      console.log($scope.OrderList[0]);*/
    };

    $scope.RemoveOrderListPopup  = function(id){
      var active =  $('[data-is='+id+']').hasClass('active');
      angular.forEach(Results.photos, function(value, key){
          if(value.id==id){
            if($scope.OrderList.indexOf(value) == -1){
              $scope.OrderList.push(value);
            };
              angular.forEach($scope.OrderList, function(value, key){
                if(value.id==id){
                  $scope.OrderList.splice(key, 1);
                };
              });
            
          };
      });
      /*console.log($scope.OrderList[0].counter = 1);
      console.log($scope.OrderList[0]);*/
    };
    
    $scope.glick = function(id,plus,minus) {
      var element = $('[data-is='+id+']');
      var smena = element.toggleClass("active");
      if(plus){
        var con = parseInt(element.find('.counter').html());
        element.find('.counter').html(' ' +(con+1) + ' ');
      };
      if(minus){
        var con = parseInt(element.find('.counter').html());
        if(con!=1){
          element.find('.counter').html(' ' +(con-1) + ' ');
        }
      };

      $scope.getCounter();
      $scope.RemoveOrderList(id);
      
    };

    

    $scope.reset = function() {
      angular.forEach($(".counter__block"), function(value, key){
          $(value).removeClass('active');
      });
      $scope.getCounter();
      $scope.RemoveOrderList();
    };

    /*open setting photo*/ 
    $scope.clickToOpen = function (id) {
      angular.forEach($scope.OrderList, function(value, key){
          if(value.id==id){
            $scope.recent = value;
        };
      });
      ngDialog.open({
        template: 'templates/layout/popup_photo.html',
        className: 'ngdialog-theme-photo',
        scope: $scope
      });
    };
    $scope.clickToOpenNext = function (id) {
      console.log(id);
      angular.forEach($scope.OrderList, function(value, key){
          if(value.id==id){
            $scope.recent = value;
        };
      });
      ngDialog.close();
      ngDialog.open({
        template: 'templates/layout/popup_photo.html',
        className: 'ngdialog-theme-photo',
        controller: 'PhotosController',
        scope: $scope,
        cache: false
      });
    };
    
    //CLICK DALEE
    $scope.Next = function () {
      ngDialog.open({
        template: 'templates/layout/order.html'+'?_=' + Math.random(),
        className: 'ngdialog-theme-order',
        scope: $scope,
        showClose: false,
        cache: false
      });
    };
    
  }
})();